describe('Personal zone page', () => {
  beforeEach(() => {
    cy.login('205596612', '24b12290f');
    cy.visit('/perosnal_zone');
  });

  it('page loaded successfully while logged in', () => {
    cy.get('header').should('have.text', 'שלום');
  });
});

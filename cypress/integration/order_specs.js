describe('The order page', () => {
  let personalDetails = {};

  // load fixture just once
  // need to store in closure variables because context is cleared before each test
  before(() => {
    cy
      .fixture('orders/good_personal_details')
      .then((data) => {
        personalDetails = data;
      });
  });

  beforeEach(() => {
    // we can put data back into the empty context before each test
    // by the time this callback executes, "before" hook has finished
    cy
      .wrap(personalDetails)
      .as('personalDetails');
    cy.visit('/');
  });

  it('load iframe in order page of the first deal', function () {
    cy
      .get('[data-cy=btn-deal-init-order]')
      .first()
      .click(); // choose first deal
    cy
      .get('[data-cy=btn-deal-choose-composition]')
      .click(); // choose the default composition
    cy
      .get('[data-cy=btn-deal-choose-room]')
      .first()
      .click(); // choose first room

    cy
      .get('[data-cy=form-order-personal-details] input[name=email]')
      .type(this.personalDetails.email);
    cy
      .get('[data-cy=form-order-personal-details] input[name=firstName]')
      .type(this.personalDetails.firstName);
    cy
      .get('[data-cy=form-order-personal-details] input[name=lastName]')
      .type(this.personalDetails.lastName);
    cy
      .get('[data-cy=form-order-personal-details] input[name=idNumber]')
      .type(this.personalDetails.id);
    cy
      .get('[data-cy=form-order-personal-details] input[name=phone]')
      .type(this.personalDetails.phone);

    cy
      .get('[data-cy=form-order-guest-details] input[name=firstName]')
      .type(this.personalDetails.firstName);
    cy
      .get('[data-cy=form-order-guest-details] input[name=lastName]')
      .type(this.personalDetails.lastName);

    cy
      .get('[data-cy=btn-order-payment-details]')
      .click();
    cy
      .getIframe('iframe[data-cy=iframe-order-pelecard]')
      .find('#submitBtn')
      .should('have.text', 'בצע תשלום ');
    // .click();
  });
});

describe('Home page', () => {
  it('successfully loads', () => {
    cy.visit('/');
  });

  it('measures page load on the home page', () => {
    cy
      .visit('/home', {
        onBeforeLoad: (win) => {
          win.performance.mark('start-loading');
        },
        onLoad: (win) => {
          win.performance.mark('end-loading');
        },
      })
      .its('performance')
      .then((performance) => {
        performance.measure('pageLoad', 'start-loading', 'end-loading');
        // Retrieve the timestamp we just created
        const measure = performance.getEntriesByName('pageLoad')[0];
        const { duration } = measure;

        assert.isAtMost(duration, 5000);
      });
  });
});

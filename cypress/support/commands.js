Cypress.Commands.add('getIframe', (selector) => {
  // get the iframe > document > body and retry until the body element is not empty
  // wraps "body" DOM element to allow chaining more Cypress commands
  cy.log('getIframe');

  return cy
    .get(selector)
    .its('0.contentDocument.body')
    .should('not.be.empty')
    .then(cy.wrap);
});

Cypress.Commands.add('login', (username, password) => {
  cy.session([username, password], () => {
    cy.visit('/');
    cy.get('[data-cy=btn-open-login-modal]').click(); // open login modal
    cy.get('[data-cy=btn-choose-login-method]').click(); // switch to username-password method
    cy.get('[data-cy=form-login] input[name=username]').type(username);
    cy.get('[data-cy=form-login] input[name=password]').type(password);
    cy.get('[data-cy=btn-login]').click();
    cy.get('header').should('have.text', 'היי');
  });
});
